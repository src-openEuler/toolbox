%define debug_package %{nil}

Name:          toolbox
Version:       0.0.99.5

%global goipath github.com/containers/%{name}

Release:       4
Summary:       Tool for interactive command line environments on Linux

License:       Apache-2.0
URL:           https://github.com/containers/toolbox

Source0:       toolbox-0.0.99.5-vendored.tar.xz
Source1:       https://github.com/cpuguy83/go-md2man/archive/v1.0.10.tar.gz
Patch0:        0001-toolbox-0.0.99.5-add-support-for-loongarch64.patch

BuildRequires: golang >= 1.20 meson >= 0.58.0
BuildRequires: pkgconfig(bash-completion) systemd
BuildRequires: gcc shadow chrpath
  
Recommends:    skopeo
Requires:      podman >= 1.4.0 containers-common

%description
Toolbox is a tool for Linux operating systems, which allows the use of
containerized command line environments. It is built on top of Podman and
other standard container technologies from OCI.

%package       tests
Summary:       Tests for toolbox.

Requires:      %{name}%{?_isa} = %{version}-%{release}
Requires:      coreutils
Requires:      grep
Requires:      bats >= 1.7.0
# for htpasswd
Requires:      httpd-tools
Requires:      openssl
Requires:      skopeo

%description   tests
The toolbox-tests package contains system tests for toolbox.


%prep
%setup -q
%patch0 -p1

GOBUILDDIR="$(pwd)/_build"
GOSOURCEDIR="$(pwd)"
if [[ ! -e "$GOBUILDDIR/bin" ]] ; then
  install -m 0755 -vd "$GOBUILDDIR/bin"
fi
if [[ ! -e "$GOBUILDDIR/src/%{goipath}" ]] ; then
  install -m 0755 -vd "$(dirname $GOBUILDDIR/src/%{goipath})"
  ln -fs "$GOSOURCEDIR" "$GOBUILDDIR/src/%{goipath}"
fi
cd "$GOBUILDDIR/src/%{goipath}"

tar -xf %SOURCE1

%build
GO_MD2MAN_PATH="$(pwd)%{_bindir}"
mkdir -p _build/bin $GO_MD2MAN_PATH
cd go-md2man-*
go build -mod=vendor -o ../_build/bin/go-md2man .
cp ../_build/bin/go-md2man $GO_MD2MAN_PATH/go-md2man
export PATH=$GO_MD2MAN_PATH:$PATH
cd -

export GO111MODULE=off
GOBUILDDIR="$(pwd)/_build"
export GOPATH="$GOBUILDDIR:%{gopath}"
export CGO_CFLAGS="%{optflags} -D_GNU_SOURCE -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64"
ln -s src/cmd cmd
ln -s src/pkg pkg
ln -s src/vendor vendor
%meson \
    -Dprofile_dir=%{_sysconfdir}/profile.d \
    -Dtmpfiles_dir=%{_tmpfilesdir} \
    -Dzsh_completions_dir=%{_datadir}/zsh/site-functions
%meson_build


%install
%meson_install

if [ -f %{buildroot}/usr/bin/toolbox ]; then
    chrpath --delete %{buildroot}/usr/bin/toolbox
fi

%files
%doc CODE-OF-CONDUCT.md NEWS README.md SECURITY.md
%license COPYING
%{_bindir}/%{name}
%{_datadir}/bash-completion
%{_datadir}/zsh
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/%{name}-*.1*
%{_mandir}/man5/%{name}.conf.5*
%config(noreplace) %{_sysconfdir}/containers/%{name}.conf
%{_sysconfdir}/profile.d/%{name}.sh
%{_tmpfilesdir}/%{name}.conf

%files tests
%{_datadir}/%{name}

%changelog
* Tue Apr 23 2024 Pengda Dou <doupengda@loongson.cn> - 0.0.99.5-4
- add support for loongarch64

* Wed Apr 10 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 0.0.99.5-3
- fix riscv spec

* Tue Apr 2 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 0.0.99.5-2
- rebase patch and fix

* Thu Jan 04 2024 duyiwei <duyiwei@kylinos.cn> - 0.0.99.5-1
- upgrade version to 0.0.99.5

* Fri Jul 14 2023 zhangxiang <zhangxiang@iscas.ac.cn> - 0.0.99-5
- add riscv64 support

* Fri Dec 09 2022 liukuo <liukuo@kylinos.cn> - 0.0.99-4
- License compliance rectification

* Thu Sep 15 2022 duyiwei <duyiwei@kylinos.cn> - 0.0.99-3
- change /usr/lib/os-release to /etc/os-release

* Tue Jun 07 2022 fushanqing <fushanqing@kylinos.cn> - 0.0.99.3-2
- update Source0

* Thu Feb 10 2022 duyiwei <duyiwei@kylinos.cn> - 0.0.99.3-1
- Package init
